package snakesandladdersrunner;

import games.snakesandladders.*;

public class SnakesAndLaddersRunner {
    public static void main(String[] args) {
        // TODO: Scanner for user input for each named player.
        // TODO: Random roll for each player to determine order, rather than entered order
        Player[] players = {
            new Player("Edmond Dantés"),
            new Player("Giovanni Bertuccio"),
            new Player("Gérard de Villefort"),
            new Player("Baron Danglars"),
            new Player("Mercédès"),
            new Player("Fernand Mondego")
        };
        
        Die die = new Die(6);
        GameBoard board = new GameBoard(100);
        
        // Taken from a board as seen on an online image
        board.addLadder(0, 37);
        board.addLadder(3, 10);
        board.addLadder(8, 22);
        board.addLadder(20, 21);
        board.addLadder(27, 56);
        board.addLadder(35, 8);
        board.addLadder(50, 16);
        board.addLadder(70, 20);
        board.addLadder(79, 20);
        board.addSnake(15, 10);
        board.addSnake(46, 21);
        board.addSnake(48, 38);
        board.addSnake(55, 3);
        board.addSnake(63, 4);
        board.addSnake(86, 63);
        board.addSnake(92, 20);
        board.addSnake(94, 20);
        board.addSnake(97, 20);
        
        TextualGameReporter reporter = new TextualGameReporter();
        Game game = new Game(board, players, die, reporter, new Rules());

        game.startGame();
        
        while(game.getState() != GameState.COMPLETED) {
            game.nextTurn();
        }
    }
}
